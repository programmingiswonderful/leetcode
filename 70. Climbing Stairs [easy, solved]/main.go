package main

import "fmt"

func main() {
	fmt.Println(climbStairs(2))
}

func climbStairs(n int) int {
	if n <= 2 {
		return n
	}
	prevPrev := 1
	prev := 2

	for i := 0; i < n-2; i++ {
		prev, prevPrev = prevPrev+prev, prev
	}

	return prev
}
