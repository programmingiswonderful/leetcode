package main

import "fmt"

func main() {
	s := "(){}[["
	fmt.Println(isValid(s))
	// s -> "40 41 123 125 91 93"
}

func isValid(s string) bool {
	lenS := len(s)
	if lenS%2 == 1 {
		return false
	}
	var sArr []byte
	lenSArr := 0
	for i := 0; i < lenS; i++ {
		currentChar := s[i]
		if currentChar == 40 || currentChar == 123 || currentChar == 91 {
			sArr = append(sArr, currentChar)
			lenSArr++
			continue
		}

		if lenSArr == 0 {
			fmt.Println("24")
			return false
		}
		lastChar := sArr[lenSArr-1]
		if !((currentChar == 41 && lastChar == 40) || (currentChar == 125 && lastChar == 123) || (currentChar == 93 && lastChar == 91)) {
			return false
		}
		sArr = sArr[:lenSArr-1]
		lenSArr--
	}

	if lenSArr != 0 {
		return false
	}
	return true
}
