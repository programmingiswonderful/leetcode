package main

import (
	"sort"
)

func main() {
	nums1 := []int{4, 0, 0, 0, 0, 0}
	m := 1
	nums2 := []int{1, 2, 3, 5, 6}
	n := 5
	merge(nums1, m, nums2, n)
}

func merge(nums1 []int, m int, nums2 []int, n int) {
	sort.Ints(append(nums1[:m], nums2...))
}
