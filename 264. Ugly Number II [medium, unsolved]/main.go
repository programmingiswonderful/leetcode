package main

import "fmt"

func main() {
	fmt.Println(nthUglyNumber(1690))
}

func nthUglyNumber(n int) int {
	deviderNums := []int{2, 3, 5}
	size := 1
	i := 2
	for ; size < n; i++ {
		isDevided := false
		for _, j := range deviderNums {
			if i%j == 0 {
				isDevided = true
				break
			}
		}
		if isDevided {
			size++
		}
	}
	return i - 1
}
