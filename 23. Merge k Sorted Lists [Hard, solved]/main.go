package main

import (
	"fmt"
	"math"
)

func main() {
	lnl := []*ListNode{
		nil,
		&ListNode{
			Val:  1,
			Next: nil,
		},
	}
	res := mergeKLists(lnl)
	for res != nil {
		fmt.Println(res.Val)
		res = res.Next
	}
}

// [[1,4,5],[1,3,4],[2,6]]

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeKLists(lists []*ListNode) *ListNode {
	lenLists := len(lists)
	if lenLists == 0 {
		return nil
	}
	if lenLists == 1 && lists[0] == nil {
		return nil
	}
	if lenLists == 1 && lists[0].Next == nil && lists[0].Val == 0 {
		return &ListNode{}
	}
	res := &ListNode{}
	head := res
	currenLenLists := 0
	var isListNil []bool
	for _, l := range lists {
		if l != nil {
			currenLenLists++
		}
	}

	for currenLenLists != 0 {
		minELem := &ListNode{Val: math.MaxInt}
		minElemIndex := 0
		isFound := false
		for i := 0; i < lenLists; i++ {
			if !isListNil[i] && lists[i] == nil {
				currenLenLists--
				isListNil[i] = true
				continue
			}
			if isListNil[i] {
				continue
			}
			if minELem.Val > lists[i].Val {
				minELem = lists[i]
				minElemIndex = i
				isFound = true
			}
		}
		if isFound {
			head.Next = minELem
			lists[minElemIndex] = lists[minElemIndex].Next
			head = head.Next
		}
	}

	return res.Next
}
