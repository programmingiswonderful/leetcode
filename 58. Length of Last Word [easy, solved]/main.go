package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(lengthOfLastWord(" "))
}

func lengthOfLastWord(s string) int {
	splittedS := strings.Split(strings.TrimRight(s, " "), " ")
	length := len(splittedS)
	return len(splittedS[length-1])
}
