package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println(addBinary("1", "111"))
}

func addBinary(a string, b string) string {
	iA := len(a) - 1
	iB := len(b) - 1
	res := ""
	r := 0
	sum := 0
	num1 := 0
	num2 := 0

	for iA != -1 || iB != -1 {
		if iA == -1 {
			num1, _ = strconv.Atoi(string(b[iB]))
			sum = num1 + r
			r = sum / 2
			res = fmt.Sprintf("%d", sum%2) + res
			iB--
			continue
		}

		if iB == -1 {
			num1, _ = strconv.Atoi(string(a[iA]))
			sum = num1 + r
			r = sum / 2
			res = fmt.Sprintf("%d", sum%2) + res
			iA--
			continue
		}

		num1, _ = strconv.Atoi(string(a[iA]))
		num2, _ = strconv.Atoi(string(b[iB]))
		sum = num1 + num2 + r
		r = sum / 2
		res = fmt.Sprintf("%d", sum%2) + res
		iA--
		iB--
	}

	if r != 0 {
		res = fmt.Sprintf("%d", r) + res
	}

	return res
}
