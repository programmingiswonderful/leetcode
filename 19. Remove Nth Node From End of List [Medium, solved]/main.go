package main

import "fmt"

func main() {
	ln := ListNode{
		Val:  1,
		Next: nil,
	}
	res := removeNthFromEnd(&ln, 1)
	for res != nil {
		fmt.Println(res.Val)
		res = res.Next
	}
}

type ListNode struct {
	Val  int
	Next *ListNode
}

func removeNthFromEnd(head *ListNode, n int) *ListNode {
	if head.Next == nil {
		return nil
	}
	realDepth := 0
	isRemoved := false
	dive(head, &realDepth, 1, n, &isRemoved)
	return head
}

func dive(head *ListNode, realDepth *int, currentDepth int, n int, isRemoved *bool) {
	if head != nil {
		*realDepth++
		dive(head.Next, realDepth, currentDepth+1, n, isRemoved)
	}

	if *realDepth-currentDepth+1 == n {
		if head.Next != nil {
			head.Val = head.Next.Val
			head.Next = head.Next.Next
			*isRemoved = true
		}
	} else if *realDepth-currentDepth == n && !*isRemoved {
		head.Next = nil
	}
	return
}
