package main

import "fmt"

func main() {
	fmt.Println(removeDuplicates([]int{1, 1, 2}))
}

func removeDuplicates(nums []int) int {
	prev := nums[0]
	k := 1
	for _, num := range nums {
		if prev != num {
			nums[k] = num
			k++
		}
		prev = num
	}
	return k
}
