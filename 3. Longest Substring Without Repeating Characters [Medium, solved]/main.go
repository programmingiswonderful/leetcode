package main

import (
	"fmt"
)

func main() {
	res := lengthOfLongestSubstring("dvdf")
	fmt.Println(res)
}

func lengthOfLongestSubstring(s string) int {
	uniqueCharsMap := make(map[byte]int)
	maxLength := 0
	currentLength := 0
	var i int = 0

	for i != len(s) {
		if v, ok := uniqueCharsMap[s[i]]; !ok {
			currentLength += 1
			uniqueCharsMap[s[i]] = i
		} else {
			uniqueCharsMap = make(map[byte]int)
			i = v
			currentLength = 0
		}

		if currentLength > maxLength {
			maxLength = currentLength
		}
		i++
	}
	return maxLength
}

// pwwkew
