package main

import "fmt"

func main() {
	n := 3
	result := generateParenthesis(n)
	fmt.Println(result)
}

func generateParenthesis(n int) []string {
	var result []string
	if n == 0 {
		return result
	}
	dfs(n, n, "", &result)
	return result
}

func dfs(left, right int, path string, result *[]string) {
	if left == 0 && right == 0 {
		*result = append(*result, path)
		return
	}
	if left > 0 {
		dfs(left-1, right, path+"(", result)
	}
	if right > 0 && left < right {
		dfs(left, right-1, path+")", result)
	}
}
