package main

import (
	"fmt"
)

func min(a, b, c int) int {
	if a <= b && a <= c {
		return a
	} else if b <= a && b <= c {
		return b
	}
	return c
}

func levenshteinDistance(str1, str2 string) int {
	len1, len2 := len(str1), len(str2)

	matrix := make([][]int, len1+1)
	for i := range matrix {
		matrix[i] = make([]int, len2+1)
	}

	for i := 0; i <= len1; i++ {
		for j := 0; j <= len2; j++ {
			if i == 0 {
				matrix[i][j] = j
			} else if j == 0 {
				matrix[i][j] = i
			} else {
				cost := 0
				if str1[i-1] != str2[j-1] {
					cost = 1
				}
				matrix[i][j] = min(
					matrix[i-1][j]+1,
					matrix[i][j-1]+1,
					matrix[i-1][j-1]+cost,
				)
			}
		}
	}

	return matrix[len1][len2]
}

func similarityPercentage(str1, str2 string) float64 {
	distance := levenshteinDistance(str1, str2)
	maxLen := float64(len(str1) + len(str2))
	similarity := (1.0 - float64(distance)/maxLen) * 100.0
	return similarity
}

func main() {
	string1 := "hello"
	string2 := "helo"
	percentage := similarityPercentage(string1, string2)
	fmt.Printf("Percentage of similarity '%s' and '%s' is: %.2f%%\n", string1, string2, percentage)
}
