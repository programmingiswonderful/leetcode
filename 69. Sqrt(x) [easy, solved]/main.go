package main

import (
	"fmt"
	"math"
	"runtime"
)

func main() {
	fmt.Println(mySqrt(int(math.Pow(2, 32)) >> 1))
	// Get the current memory usage
	m := &runtime.MemStats{}
	runtime.ReadMemStats(m)

	// Print the memory usage
	fmt.Printf("Memory usage: %d bytes\n", m.Alloc)
}

func mySqrt(x int) int {
	if x == 0 {
		return 0
	}
	if x == 1 {
		return 1
	}
	l := 0
	r := x

	for l <= r {
		median := (l + r) / 2
		dm := median * median

		if dm == x {
			return median
		}

		if dm < x {
			l = median + 1
		} else {
			r = median - 1
		}
	}
	return r
}
