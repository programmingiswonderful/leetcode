package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(convert("PAYPALISHIRING", 3))
}

func convert(s string, numRows int) string {
	if numRows == 1 {
		return s
	}

	var res strings.Builder
	rows := make([]string, numRows)

	r := 0
	lenS := len(s)
	forward := false

	for i := 0; i < lenS; i++ {
		if r == 0 || r == numRows-1 {
			forward = !forward
		}

		rows[r] += string(s[i])

		if forward {
			r++
			continue
		}
		r--
	}

	for i := 0; i < len(rows); i++ {
		res.WriteString(rows[i])
	}

	return res.String()
}
