package main

import "fmt"

func main() {
	fmt.Println(longestValidParentheses("()(()"))
}

func longestValidParentheses(s string) int {
	if len(s) == 0 {
		return 0
	}
	maxCount := 0
	counter := 0
	openers := make([]byte, 0)
	lenOpeners := 0

	for i := 0; i < len(s); i++ {
		if s[i] == 40 {
			openers = append(openers, 40)
			lenOpeners++
		} else {
			if lenOpeners > 0 {
				openers = openers[:lenOpeners-1]
				lenOpeners--
				counter += 2
			} else {
				if counter > maxCount {
					maxCount = counter
					counter = 0
				}
			}
		}
	}
	if counter > maxCount {
		maxCount = counter
	}
	return maxCount
}
