package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	fmt.Println(reverse(-9463847412))
}

func reverse(x int) int {
	s := strconv.Itoa(x)
	var res string
	var isNegative bool
	var greater int8
	minInt32Str := fmt.Sprintf("%d", math.MaxInt32+1)
	maxInt32Str := fmt.Sprintf("%d", math.MaxInt32)

	if s[0] == '-' {
		isNegative = true
	}

	if isNegative {
		res = reverseStr(s[1:])
	} else {
		res = reverseStr(s)
	}

	if isNegative {
		greater = compareTwoUnsignedNumbersStr(minInt32Str, res)
		if greater == 1 || greater == 0 {
			res = "-" + res
			i, _ := strconv.Atoi(res)
			return i
		}
		return 0
	}

	greater = compareTwoUnsignedNumbersStr(maxInt32Str, res)
	if greater == 1 || greater == 0 {
		i, _ := strconv.Atoi(res)
		return i
	}
	return 0
}

func reverseStr(str string) string {
	runeStr := []rune(str)
	lenStr := len(runeStr)
	lenMidd := lenStr / 2

	for i := 0; i < lenMidd; i++ {
		runeStr[i], runeStr[lenStr-1-i] = runeStr[lenStr-1-i], runeStr[i]
	}

	return string(runeStr)
}

func compareTwoUnsignedNumbersStr(a string, b string) int8 {
	lenA := len(a)
	lenB := len(b)

	if lenA > lenB {
		return 1
	} else if lenB > lenA {
		return -1
	}

	for i := 0; i < lenA; i++ {
		if a[i] > b[i] {
			return 1
		} else if b[i] > a[i] {
			return -1
		}
	}

	return 0
}
