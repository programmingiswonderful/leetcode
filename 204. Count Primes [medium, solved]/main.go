package main

import (
	"fmt"
	"time"
)

func main() {
	n := 636381

	fmt.Println(time.Now())

	fmt.Println("v1: ", countPrimes(n))
	// fmt.Println("v2: ", countPrimesV2(n))

	fmt.Println(time.Now())
}

func countPrimes(n int) int {
	if n <= 2 {
		return 0
	}
	count := 0
	primesArr := make([]bool, n+1, n+1)

	for i := 2; i < n; i++ {
		primesArr[i] = true
	}

	for i := 2; i < n; i++ {
		if primesArr[i] {
			for j := i + i; j < n; j += i {
				primesArr[j] = false
			}
		}
	}

	for i := 2; i < n; i++ {
		if primesArr[i] {
			count++
		}
	}

	return count
}

func countPrimesV2(n int) int {
	if n <= 1 {
		return 0
	}
	if n == 2 || n == 3 {
		return n - 2
	}
	primes := []int{2, 3}
	i := 5
	sqrtI := 3
	count := 2

	for ; i < n; i++ {
		if sqrtI*sqrtI < i {
			sqrtI++
		}

		isPrime := true
		for _, j := range primes {
			if j > sqrtI {
				break
			}
			if i%j == 0 {
				isPrime = false
			}
		}
		if isPrime {
			primes = append(primes, i)
			count++
		}
	}

	return count
}
