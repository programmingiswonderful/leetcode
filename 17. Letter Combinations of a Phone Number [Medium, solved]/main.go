package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println(letterCombinations("2"))
}

type Params struct {
	numberLetterMap []string
	result          []string
	digits          string
	lenDigits       int
}

func letterCombinations(digits string) []string {
	if len(digits) == 0 {
		return []string{}
	}
	params := Params{
		numberLetterMap: []string{"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"},
		result:          make([]string, 0),
		digits:          digits,
		lenDigits:       len(digits),
	}
	combination(0, "", &params)
	return params.result
}

func combination(lenRes int, res string, params *Params) {
	if lenRes == params.lenDigits {
		params.result = append(params.result, res)
		return
	}

	iter, _ := strconv.Atoi(string(params.digits[lenRes]))

	for _, v := range params.numberLetterMap[iter] {
		combination(lenRes+1, res+string(v), params)
	}
}
