package main

import "fmt"

func main() {
	fmt.Println(singleNumber([]int{30000, 500, 100, 30000, 100, 30000, 100}))
}

func singleNumber(nums []int) int {
	ones, twos := 0, 0

	for _, num := range nums {
		ones = (ones ^ num) &^ twos
		twos = (twos ^ num) &^ ones
	}

	return ones
}
