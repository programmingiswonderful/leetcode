package main

import "fmt"

func main() {
	fmt.Println(plusOne([]int{9, 9}))
}

func plusOne(digits []int) []int {
	var r int
	var rpd int
	for i := len(digits) - 1; i >= 0; i-- {
		if i == len(digits)-1 {
			rpd = digits[i] + 1
		} else {
			rpd = digits[i] + r
		}
		digits[i] = rpd % 10
		r = rpd / 10
	}

	if r != 0 {
		res := []int{r}
		res = append(res, digits...)
		return res
	}
	return digits
}

func addToDigits(digits []int, i int, num int) int {
	var rpd int
	rpd = digits[i] + num
	digits[i] = rpd % 10
	return rpd / 10
}
