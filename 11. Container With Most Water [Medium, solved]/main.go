package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println(time.Now().String())
	fmt.Println(maxArea([]int{1, 0, 0, 0, 0, 0, 0, 2, 2}))
	fmt.Println(time.Now().String())
}

func maxArea(height []int) int {
	var left, right, distBetween, area, maximumArea int
	lenNewHeight := len(height)
	leftIndex := 0
	rightIndex := lenNewHeight - 1
	for leftIndex != rightIndex {
		left = height[leftIndex]
		right = height[rightIndex]
		distBetween = rightIndex - leftIndex

		isLeftGreater, shorterHeight := compareBounds(left, right)
		area = shorterHeight * distBetween
		if area > maximumArea {
			maximumArea = area
		}

		if isLeftGreater {
			rightIndex--
		} else {
			leftIndex++
		}
	}
	return maximumArea
}

func compareBounds(left int, right int) (bool, int) {
	if left > right {
		return true, right
	}

	return false, left
}
