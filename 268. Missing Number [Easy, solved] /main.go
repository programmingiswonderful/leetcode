package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println(missingNumber([]int{0, 1}))
}

func missingNumber(nums []int) int {
	lenNums := len(nums)
	sumNums := 0
	lenMidd := lenNums / 2
	isLenOdd := lenNums%2 == 1
	fmt.Println(time.Now().String())
	for i := 0; i < lenMidd; i++ {
		sumNums += nums[i] + nums[lenNums-1-i]
	}
	fmt.Println(time.Now().String())
	if isLenOdd {
		sumNums += nums[lenMidd]
	}
	needSum := ((lenNums + 1) * lenNums) / 2

	return needSum - sumNums
}
