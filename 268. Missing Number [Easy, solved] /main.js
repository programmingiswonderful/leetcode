var missingNumber = function(nums) {
    let n = nums.length;
    let sum = nums.reduce((acc, current) => acc+current, 0);
    let needSum = n * (n+1) / 2;
    return needSum - sum;
};
console.log(missingNumber([0, 1]));