package main

import "fmt"

func main() {
	nums := []int{10, 12, 19, 14}
	fmt.Println(maximumSum(nums))
}

func maximumSum(nums []int) int {
	maxSum := -1
	sumsMap := make(map[int]int)

	for _, num := range nums {
		sum := sumOfDigits(num)
		if prevNum, ok := sumsMap[sum]; ok {
			numsSum := prevNum + num
			if maxSum < numsSum {
				maxSum = numsSum
			}
			if num > prevNum {
				sumsMap[sum] = num
			}
		} else {
			sumsMap[sum] = num
		}
	}
	return maxSum
}

func sumOfDigits(num int) int {
	sum := 0
	for num != 0 {
		sum += num % 10
		num /= 10
	}
	return sum
}
