package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(threeSum([]int{0, 0, 0, 0, 0, 0}))
	// [ -4, -1, -1, 0, 1, 2 ]
	// -4 + 1 + 2
}

func threeSum(nums []int) [][]int {
	sort.Ints(nums)
	lenNums := len(nums)
	currentNum := nums[0]
	res := make([][]int, 0)
	for i := 0; i < lenNums-2; i++ {
		if i != 0 && nums[i] == currentNum {
			continue
		} else {
			currentNum = nums[i]
		}
		left := i + 1
		right := lenNums - 1
		for left < right {
			threeSumm := currentNum + nums[left] + nums[right]
			if threeSumm > 0 {
				right--
			} else if threeSumm < 0 {
				left++
			} else {
				res = append(res, []int{currentNum, nums[left], nums[right]})
				left++
				for nums[left] == nums[left-1] && left < right {
					left++
				}
			}
		}
	}
	return res
}
