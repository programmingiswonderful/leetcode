package main

import "fmt"

func main() {
	fmt.Println(longestCommonPrefix([]string{"dog", "racecar", "car"}))
}

func longestCommonPrefix(strs []string) string {
	var res string
	var currentStrLength int
	minStrLength := len(strs[0])

	for _, str := range strs {
		currentStrLength = len(str)
		if currentStrLength < minStrLength {
			minStrLength = currentStrLength
		}
	}

	for i := 0; i < minStrLength; i++ {
		currentComparableChar := strs[0][i]
		for _, str := range strs {
			if str[i] != currentComparableChar {
				return res
			}
		}
		res += string(currentComparableChar)
	}
	return res
}
