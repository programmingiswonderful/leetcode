package main

import (
	"fmt"
	"math"
)

func main() {
	num := 99
	maxNum := 100

	guessCount := binarySearch(num, maxNum)
	fmt.Println(guessCount)
}

func binarySearch(num int, maxNum int) (guessCount int) {
	maxNumF := float64(maxNum)
	maxGuess := int(math.Ceil(math.Log2(maxNumF)))
	fmt.Printf("Maximum guesses: %d\n", maxGuess)

	left := 0
	right := maxNum

	for {
		current := (left + right) / 2
		guessCount++
		fmt.Println(current)
		if current == num {
			return
		}

		if current > num {
			right = current
		}

		if current < num {
			left = current
		}
	}
}
