package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func main() {
	s := "-2147483647"
	// maxInt32Str := fmt.Sprintf("%d", math.MaxInt32)
	// fmt.Println(compareTwoUnsignedNumbersStr(s, maxInt32Str))
	// fmt.Println(strings.Trim(s, " "))
	fmt.Println(myAtoi(s))
	// '0' -> '9' => 48 -> 57
	// '-' => 45
}

func myAtoi(s string) int {
	s = strings.Trim(s, " ")
	lenS := uint8(len(s))

	if lenS == 0 {
		return 0
	}

	firstChar := s[0]
	isNegative := firstChar == 45
	isPositive := firstChar == 43

	maxInt32Str := fmt.Sprintf("%d", math.MaxInt32)
	minInt32Str := fmt.Sprintf("%d", math.MaxInt32+1)

	if !(firstChar >= 48 && firstChar <= 57) && firstChar != 45 && firstChar != 43 {
		return 0
	}
	res := ""
	var i uint8

	if isNegative || isPositive {
		i = 1
	} else {
		i = 0
	}

	for ; i < lenS; i++ {
		if !(s[i] >= 48 && s[i] <= 57) {
			break
		}

		res += string(s[i])
	}

	if res == "" {
		return 0
	}

	if isNegative {
		greater := compareTwoUnsignedNumbersStr(res, minInt32Str)
		if greater == -1 {
			res = "-" + res
			resInt, _ := strconv.Atoi(res)
			return resInt
		} else {
			return math.MinInt32
		}
	} else {
		greater := compareTwoUnsignedNumbersStr(res, maxInt32Str)
		if greater == -1 {
			resInt, _ := strconv.Atoi(res)
			return resInt
		} else {
			return math.MaxInt32
		}
	}
}

func compareTwoUnsignedNumbersStr(a string, b string) int8 {
	a = strings.TrimLeft(a, "0")
	b = strings.TrimLeft(b, "0")
	lenA := len(a)
	lenB := len(b)

	if lenA > lenB {
		return 1
	} else if lenB > lenA {
		return -1
	}

	for i := 0; i < lenA; i++ {
		if a[i] > b[i] {
			return 1
		} else if b[i] > a[i] {
			return -1
		}
	}

	return 0
}
