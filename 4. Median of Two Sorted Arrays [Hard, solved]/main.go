package main

import (
	"fmt"
	"math"
)

func main() {
	l1 := []int{}
	l2 := []int{2}
	res := findMedianSortedArrays(l1, l2)

	fmt.Println(res)
}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	iterNums1 := 0
	iterNums2 := 0
	lenNums1 := len(nums1)
	lenNums2 := len(nums2)
	totalLen := lenNums1 + lenNums2
	currentElem := 0

	var median float64 = 0
	medianIndex := int(math.Ceil(float64(totalLen) / 2))
	needMedianCount := 0
	foundMedianCount := 0

	if totalLen%2 == 0 {
		needMedianCount = 2
	} else {
		needMedianCount = 1
	}
	for {
		if iterNums1+iterNums2 == medianIndex || iterNums1+iterNums2 == medianIndex+1 {
			median += float64(currentElem)
			foundMedianCount += 1
			if foundMedianCount == needMedianCount {
				break
			}
		}

		if iterNums1 >= lenNums1 {
			currentElem = nums2[iterNums2]
			iterNums2++
			continue
		}
		if iterNums2 >= lenNums2 {
			currentElem = nums1[iterNums1]
			iterNums1++
			continue
		}

		if nums1[iterNums1] < nums2[iterNums2] {
			currentElem = nums1[iterNums1]
			iterNums1++
		} else {
			currentElem = nums2[iterNums2]
			iterNums2++
		}
	}

	if needMedianCount == 2 {
		return median / 2
	}
	return median
}

// 123 456
//
