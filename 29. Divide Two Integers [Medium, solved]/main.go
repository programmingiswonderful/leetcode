package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(divide(-2147483648, -1))
}

func divide(dividend int, divisor int) int {
	res := 0
	abs1 := math.Abs(float64(dividend))
	abs2 := math.Abs(float64(divisor))
	isResNeg := false
	if dividend < 0 {
		isResNeg = true
	}
	if divisor < 0 {
		isResNeg = isResNeg != true
	}

	if abs2 != 1 {
		for abs1 >= abs2 {
			abs1 -= abs2
			res++
		}
	} else {
		if abs1 > math.MaxInt32 && !isResNeg {
			res = math.MaxInt32
		} else {
			res = int(abs1)
		}
	}

	if isResNeg {
		res = 0 - res
	}
	return res
}
