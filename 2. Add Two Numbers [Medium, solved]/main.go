package main

import "fmt"

/**
 * Definition for singly-linked list.
**/

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	l1 := &ListNode{
		Val: 9,
		Next: &ListNode{
			Val: 9,
			Next: &ListNode{
				Val: 9,
				Next: &ListNode{
					Val: 9,
					Next: &ListNode{
						Val: 9,
						Next: &ListNode{
							Val: 9,
							Next: &ListNode{
								Val: 9,
							},
						},
					},
				},
			},
		},
	}

	l2 := &ListNode{
		Val: 9,
		Next: &ListNode{
			Val: 9,
			Next: &ListNode{
				Val: 9,
				Next: &ListNode{
					Val: 9,
				},
			},
		},
	}

	res := addTwoNumbers(l1, l2)
	for res != nil {
		fmt.Printf("%d ", res.Val)
		res = res.Next
	}
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	carry := 0
	value := 0
	res := &ListNode{}
	current := res
	for l1 != nil || l2 != nil || carry != 0 {
		if l1 == nil && l2 == nil {
			current.Next = &ListNode{Val: carry, Next: nil}
			carry = 0
			current = current.Next
			continue
		}

		if l1 == nil {
			value = (l2.Val + carry) % 10
			carry = (l2.Val + carry) / 10
			current.Next = &ListNode{Val: value}
			current = current.Next
			l2 = l2.Next
			continue
		}

		if l2 == nil {
			value = (l1.Val + carry) % 10
			carry = (l1.Val + carry) / 10
			current.Next = &ListNode{Val: value}
			current = current.Next
			l1 = l1.Next
			continue
		}

		value = (l1.Val + l2.Val + carry) % 10
		carry = (l1.Val + l2.Val + carry) / 10
		current.Next = &ListNode{Val: value}
		current = current.Next
		l1 = l1.Next
		l2 = l2.Next
	}
	return res.Next
}
